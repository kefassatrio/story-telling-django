from django.shortcuts import render, redirect
from . import forms
from .models import Jadwal

# Create your views here.

def jadwal_create(request):
    if request.method == "POST":
        form = forms.CreateJadwal(request.POST)

        if form.is_valid():   
            jadwal_baru = form.save(commit=False)
            jadwal_baru.day = jadwal_baru.date.strftime("%A")
            jadwal_baru.save()
        
        return redirect("jadwal:jadwal_create")
    else:
        form = forms.CreateJadwal()
        return render(request, "jadwal/jadwal_create.html", {"form" : form, "schedules": Jadwal.objects.all().order_by("date")})

def jadwal_delete(request, id):
    jadwal = Jadwal.objects.get(id=id)
    jadwal.delete()
    return redirect("jadwal:jadwal_create")