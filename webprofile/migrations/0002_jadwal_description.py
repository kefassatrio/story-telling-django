# Generated by Django 2.2.5 on 2019-10-02 06:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webprofile', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='jadwal',
            name='description',
            field=models.TextField(default='', max_length=200),
        ),
    ]
