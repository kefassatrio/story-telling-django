from django.urls import path
from . import views

app_name = 'webprofile'
urlpatterns = [
    path('', views.index, name="profile"),
    path('bonus/', views.bonus, name="bonus")
]