from django import forms
from . import models
from django.utils import timezone
from datetime import datetime

class TimeInput(forms.TimeInput):
    input_type = "time"

class DateInput(forms.DateInput):
    input_type = "date"
    input_formats = ['%d %b %Y']


class CreateJadwal(forms.ModelForm):
    class Meta:
        model = models.Jadwal
        fields = ['judul_jadwal', 'tempat', 'kategori', 'date', 'time', 'description']

        widgets = {
            "time": TimeInput,
            "date": DateInput
        }

        
# class CreateJadwal(forms.Form):
#     judul_jadwal = forms.CharField(max_length=50)
#     tempat = forms.CharField(max_length=100)
#     tempat = forms.ChoiceField(max_length=100)
#     date = forms.DateField(required=False, initial= timezone.now, widget= DateInput(format='%d/%m/%Y'),input_formats=('%d/%m/%Y'))
#     time = forms.TimeField(required=False, widget=TimeInput)
#     description = forms.CharField(max_length=100, required=False)
