from django.shortcuts import render
from datetime import datetime, date


# Enter your name here
title = 'Kefas Satrio' # TODO Implement this
mhs_name = "Kefas Satrio Bangkit Solideantyo"
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 4, 24) #TODO Implement this, format (Year, Month, Date)
npm = 1806204972 # TODO Implement this
universitas = "Universitas Indonesia"
# Create your views here.
def index(request):
    response = {'title': title, 'full_name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'universitas': universitas}
    return render(request, 'index.html', response)

def bonus(request):
    return render(request, 'bonus.html', {'title': "F.A.Q."})

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

