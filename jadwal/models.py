from django.db import models
import django.utils.timezone

# Create your models here.
class Jadwal(models.Model):
    KATEGORI = [
    ('urgent', 'urgent'),
    ('event', 'event'),
    ('personal', 'personal')
]

    judul_jadwal = models.CharField(max_length=100)
    tempat = models.CharField(max_length=100, null=True)
    kategori = models.CharField(
        max_length=8,
        choices=KATEGORI,
        default='personal',
    )
    # date_time = models.DateTimeField(auto_now_add = False, default = django.utils.timezone.now)
    date = models.DateField(auto_now_add = False, default = django.utils.timezone.now)
    day = models.CharField(max_length=10, default=None, null=True)
    time = models.TimeField(auto_now_add = False, default = django.utils.timezone.now)



    description = models.CharField(max_length=100, default = "")

    def __str__(self):
        return self.judul_jadwal