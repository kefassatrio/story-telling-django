# Generated by Django 2.2.5 on 2019-10-02 06:21

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Jadwal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('judul_jadwal', models.CharField(max_length=100)),
                ('tanggal', models.DateField(default=django.utils.timezone.now)),
                ('waktu', models.TimeField(default=django.utils.timezone.now)),
            ],
        ),
    ]
