from django.urls import path
from . import views

app_name = 'jadwal'
urlpatterns = [
    # path('', views.jadwal_view, name="jadwal_view"),
    path('', views.jadwal_create, name="jadwal_create"),
    path('delete/<id>/', views.jadwal_delete, name="jadwal_delete")
]